package game;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//Help from http://www.ntu.edu.sg/home/ehchua/programming/java/JavaGame_TicTacToe.html#TTTGraphicsAdvanceOO
public class Display extends JFrame {

	public static final int ROWS = 8;
	public static final int COLUMNS = 8;
	public static final int CELL_SIZE = 50; // One square

	public static final int CANVAS_WIDTH = CELL_SIZE * COLUMNS; // Board's width
	public static final int CANVAS_HEIGHT = CELL_SIZE * ROWS; // Board's height
	public static final int GRID_WIDTH = 3; // Black line's width
	public static final int CELL_PADDING = CELL_SIZE / 10; // Space between line
															// and button
	public static final int SYMBOL_SIZE = CELL_SIZE - CELL_PADDING * 2; // Button
																		// size

	// Enumeration (inner class) to represent the various states of the game
	private static JLabel statusBar;
	private DrawCanvas canvas; // Drawing canvas (JPanel) for the game board
	private static Othello othello = new Othello();

	public Display() {
		canvas = new DrawCanvas(); // Construct a drawing canvas (a JPanel)
		canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
		canvas.addMouseListener(new MouseAdapter() {
			// Gets mouse position when player clicked
			public void mouseClicked(MouseEvent e) {
				System.out.println("Click!");
				int mouseX = e.getX();
				int mouseY = e.getY();
				// Number of row and column
				int myRow = mouseY / CELL_SIZE;
				int myColumn = mouseX / CELL_SIZE;
				Position pos = new Position(myRow, myColumn);
				othello.move(pos);
				repaint(); // Call-back paintComponent().
			}
		});

		// Game status message
		statusBar = new JLabel("  ");
		statusBar.setFont(new Font(Font.SERIF, Font.BOLD, 18));
		statusBar.setBorder(BorderFactory.createEmptyBorder(4, 5, 4, 5));

		// Adds gameboard and statusbar in a window
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(canvas, BorderLayout.CENTER);
		cp.add(statusBar, BorderLayout.SOUTH);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Othello");
		setVisible(true);
		pack(); // pack all the components in this JFrame
	}

	// Inner class DrawCanvas (extends JPanel) used for custom graphics drawing.
	class DrawCanvas extends JPanel {
		@Override
		public void paintComponent(Graphics g) { // invoke with repaint()
			super.paintComponent(g);
			Field board[][] = othello.board.getBoard();
			setBackground(Color.GREEN);

			// Draws black row and column lines
			g.setColor(Color.BLACK);
			for (int row = 0; row <= ROWS; ++row) {
				g.fillRect(0, CELL_SIZE * row, CANVAS_WIDTH, GRID_WIDTH);
			}
			for (int col = 0; col <= COLUMNS; ++col) {
				g.fillRect(CELL_SIZE * col, 0, GRID_WIDTH, CANVAS_HEIGHT);
			}

			// Draws black and white buttons and also where is possible to put a button
			for (int row = 0; row < ROWS; ++row) {
				for (int col = 0; col < COLUMNS; ++col) {
					int x1 = col * CELL_SIZE + CELL_PADDING;
					int y1 = row * CELL_SIZE + CELL_PADDING;
					if (board[row][col] == Field.BLACK) {
						g.setColor(Color.BLACK);
						g.fillOval(x1, y1, SYMBOL_SIZE, SYMBOL_SIZE);
					} else if (board[row][col] == Field.WHITE) {
						g.setColor(Color.WHITE);
						g.fillOval(x1, y1, SYMBOL_SIZE, SYMBOL_SIZE);
					} else if (board[row][col] == Field.POSSIBLE) {
						g.setColor(Color.RED);
						g.drawOval(x1, y1, SYMBOL_SIZE, SYMBOL_SIZE);
					}
				}
			}
			GameStatus();
		}
	}

	public static void GameStatus() {
		// Writes a message whose turn it is
		GameState currentState = othello.getCurrentState();
		Field currentPlayer = othello.getCurrentPlayer();
		if (currentState == GameState.PLAYING) {
			statusBar.setForeground(Color.BLACK);
			if (currentPlayer == Field.BLACK) {
				statusBar.setText("Black's Turn");
			} else {
				statusBar.setText("White's Turn");
			}
			// Writes a message who won
		} else if (currentState == GameState.DRAW) {
			statusBar.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
			statusBar.setForeground(Color.RED);
			statusBar.setText("It's a draw! Click to play again.");
		} else if (currentState == GameState.BLACK_WON) {
			statusBar.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
			statusBar.setForeground(Color.RED);
			statusBar.setText("Black won the game! Click to play again.");
		} else if (currentState == GameState.WHITE_WON) {
			statusBar.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
			statusBar.setForeground(Color.RED);
			statusBar.setText("White won the game! Click to play again.");
		}
	}

}
