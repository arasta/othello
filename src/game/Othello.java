package game;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

public class Othello extends JPanel {
	// Help from https://martin-thoma.com/images/2012/04/minted-source-code.pdf
	public GameState currentState = GameState.PLAYING; // the current game state
	private Field currentPlayer = Field.BLACK;
	public final Board board; // Game board
	// VECTORS show all eight directions, where we can change opponent buttons
	private final int[][] VECTORS = { { -1, -1 }, { 0, -1 }, { 1, -1 }, { -1, 0 }, { 1, 0 }, { -1, 1 }, { 0, 1 },
			{ 1, 1 } };

	public Othello() {
		this.board = new Board(8, 8);
		initialize();

	}

	// Default gamestate in the beginning of the game - 2 black and 2 white
	// buttons
	private void initialize() {
		for (int row = 0; row < board.width; row++) {
			for (int col = 0; col < board.height; col++) {
				if (row == 3 && col == 3 || row == 4 && col == 4) {
					board.set(row, col, Field.BLACK);
				} else if (row == 4 && col == 3 || row == 3 && col == 4) {
					board.set(row, col, Field.WHITE);
				} else {
					board.set(row, col, Field.EMPTY);
				}

			}

		}

	}

	// Is game finished or not
	public GameState getCurrentState() {
		return currentState;
	}

	// Whose turn it is
	public Field getCurrentPlayer() {
		return currentPlayer;
	}

	// Who can play, switches players if there are no possible moves for one
	// player. Does the move - puts button on board and turns opponents buttons
	// to right color
	public void move(Position pos) {
		if (currentState != GameState.PLAYING) {
			restart();
		} else if (isMovePossible(currentPlayer) && numberOfSwitches(currentPlayer, pos) > 0) {
			board.set(pos, currentPlayer);
			switchPieces(pos, currentPlayer);
			toEmpty();
			nextPlayer();
			if (!isMovePossible(currentPlayer)) {
				if (isMovePossible(getWaitingPlayer())) {
					toEmpty();
					nextPlayer();
				} else {
					gameFinished();
				}
			}
		}
	}

	// Method that switces opponent buttons to currentPlayer buttons
	private void switchPieces(Position pos, Field player) {
		int switches;
		for (int[] direction : VECTORS) {
			switches = numberOfSwitches(player, pos, direction[0], direction[1]);
			for (int i = switches; i > 0; i--) {
				board.set(pos.x + i * direction[0], pos.y + i * direction[1], currentPlayer);
			}
		}

	}

	// Can the player move (is there a place where player can put his button)
	private boolean isMovePossible(Field player) {
		boolean isMovePossible = false;
		if (possibleMoves(player).size() > 0) {
			isMovePossible = true;
		}
		return isMovePossible;
	}

	// How many possible positions there are for currentPlayer buttons
	public List<Position> possibleMoves(Field Player) {
		Position pos;
		List<Position> possibleMoves = new ArrayList<Position>();
		for (int x = 0; x < board.width; x++) {
			for (int y = 0; y < board.height; y++) {
				pos = new Position(x, y);
				if (isPositionValid(pos)) {
					board.set(x, y, Field.POSSIBLE);
					possibleMoves.add(pos);
				}
			}
		}
		return possibleMoves;
	}

	// Makes possible places empty before next player starts to play
	public void toEmpty() {
		for (int x = 0; x < board.width; x++) {
			for (int y = 0; y < board.height; y++) {
				if (board.get(x, y) == Field.POSSIBLE) {
					board.set(x, y, Field.EMPTY);
				}
			}
		}
	}

	// Is button position on board and the position was empty before and in that
	// position is possible to switch opponents buttons
	private boolean isPositionValid(Position pos) {
		boolean isPositionValid = false;
		if (!board.isPositionOnBoard(pos)) {
			return false;
		} else if (board.get(pos) != Field.EMPTY && board.get(pos) != Field.POSSIBLE) {
			return false;
		} else if (numberOfSwitches(currentPlayer, pos) > 0) {
			isPositionValid = true;
		}

		return isPositionValid;
	}

	// How many switches is possible in all 8 directions (How many buttons will
	// be changed with this move)
	public int numberOfSwitches(Field player, Position pos) {
		int answer = 0;
		int number = 0;
		Position newPos;
		for (int[] direction : VECTORS) {
			for (int i = 1;; i++) {
				newPos = new Position(pos.x + i * direction[0], pos.y + i * direction[1]);
				if (board.isPositionOnBoard(newPos)) {
					if (board.get(newPos) == getWaitingPlayer()) {
						number++;
					} else if (board.get(newPos) == Field.EMPTY || board.get(newPos) == Field.POSSIBLE) {
						number = 0;
						break;
					} else {
						answer = answer + number;
						break;
					}
				} else {
					number = 0;
					break;
				}
			}
		}
		return answer;
	}

	// How many switches is possible in one direction (opponent buttons to
	// currentPlayer buttons)
	private int numberOfSwitches(Field player, Position pos, int xDir, int yDir) {
		int number = 0;
		Position newPos;
		for (int i = 1;; i++) {
			newPos = new Position(pos.x + i * xDir, pos.y + i * yDir);
			if (board.isPositionOnBoard(newPos)) {
				if (board.get(newPos) == getWaitingPlayer()) {
					number++;
				} else if (board.get(newPos) == Field.EMPTY || board.get(newPos) == Field.POSSIBLE) {
					number = 0;
					break;
				} else {
					break;
				}
			} else {
				number = 0;
				break;
			}
		}
		return number;
	}

	// When game is finished then it counts, who won
	private void gameFinished() {
		if (board.whiteNumber() > board.blackNumber()) {
			currentState = GameState.WHITE_WON;
		} else if (board.whiteNumber() < board.blackNumber()) {
			currentState = GameState.BLACK_WON;
		} else {
			currentState = GameState.DRAW;
		}
	}

	// Who is opponent - white or black
	private Field getWaitingPlayer() {
		return currentPlayer == Field.BLACK ? Field.WHITE : Field.BLACK;
	}

	// If currentPlayer is black, then nextPlayer is white, else nextPlayer is
	// black (currentPlayer white)
	private void nextPlayer() {
		currentPlayer = (currentPlayer == Field.BLACK) ? Field.WHITE : Field.BLACK;

	}

	// When game restarts then first player is black and status is playing
	private void restart() {
		currentPlayer = Field.BLACK;
		currentState = GameState.PLAYING;
		initialize();
	}
}