package game;

import javax.swing.SwingUtilities;

public class Game {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			private Display display;

			@Override
			public void run() {
				this.display = new Display();

			}
		});
	}
}