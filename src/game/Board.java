package game;

public class Board {
	public int height;
	public int width;
	private Field[][] board;

	//Makes a board with given height and width
	public Board(int width, int height) {
		this.height = height;
		this.width = width;
		setBoard(new Field[height][width]);
	}

	public Field[][] getBoard() {
		return board;
	}

	public void setBoard(Field[][] board) {
		this.board = board;
	}

	// Is the button on board
	public boolean isPositionOnBoard(Position pos) {
		boolean isPositionOnBoard = false;
		if (pos.y >= 0 && pos.y < width) {
			if (pos.x >= 0 && pos.x < height) {
				isPositionOnBoard = true;
			}
		}
		return isPositionOnBoard;

	}

	public Field get(Position pos) {
		return board[pos.x][pos.y];
	}

	public Field get(int x, int y) {
		return board[x][y];
	}

	public void set(int x, int y, Field field) {
		this.board[x][y] = field;
	}

	public void set(Position pos, Field field) {
		this.board[pos.x][pos.y] = field;
	}

	// How many white buttons are on the board
	public int whiteNumber() {
		int white = 0;
		for (Field[] array : board) {
			for (Field element : array) {
				if (element == Field.WHITE) {
					white++;
				}
			}
		}
		return white;
	}

	// How many black buttons are on the board
	public int blackNumber() {
		int black = 0;
		for (Field[] array : board) {
			for (Field element : array) {
				if (element == Field.BLACK) {
					black++;
				}
			}
		}
		return black;
	}

}
