package game;

public enum GameState {
	// Is players playing the game or who has won
	PLAYING, DRAW, BLACK_WON, WHITE_WON
}