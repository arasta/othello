package game;

public class Position {
	public int x;
	public int y;

	// What is a position
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
