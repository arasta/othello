package game;

public enum Field {
	// What is in a cell - which button, empty or is it possible position for player button
	EMPTY, BLACK, WHITE, POSSIBLE
}